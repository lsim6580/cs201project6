A mock printer queue project. the emphases was polymorphism and pointers

Name: 	Luke Simmons

Course:     CS 201
Program: 	Program 6
Due Date: 	4/19/2015
Description: 	Keep an array of files and either delete or print them.
Inputs: 	inputfile: will prompt the user for an input file
Outputs: 	fileoutput
Algorithm: 	
Prompt the user for the file of file names.
get the first word from the line.
if the word is ADD
    parse the word to get the extension
    if the extension is doc the file is a word document
        create a new instance of the Word class and read in the data and add the document to the print queue
    if the extension is htm the file is an Html document
        create a new instance of the Html Document and read the data and add the document to the print queue
    if the extension is xls the file is an excel document 
        create a new instance if the Excel class and read in the data and add the document to the print queue
if the word is PRINTALL
    output all of the documents to the output file.
if the word is CANCELALL 
    delete all of the documents in the print Array