#include <iostream>
#include <fstream>
#include <string>
#include "Document.h"
#include "Html.h" 
using namespace std;

Html::Html()
{
    type = "Html Document";
    Url = "";
}
string Html::getUrl()
{
    return Url;
}
void Html::setUrl(string word)
{
    Url = word;
}
void Html::readdata(string file)
{
    ifstream fin(file);
    string line;
    getline(fin, Title);
    getline(fin, Url);
    while (getline(fin, line))
        Content += line + '\n';
    fin.close();

}
void Html::printdata(string file)
{
    ofstream fout(file,ios::app);
    for (int k = 0; k < 40; k++)
        fout << "=";
    fout << endl;
    fout << "Title: " << Title << endl;
    fout << type << endl;
    fout << "Url: " << Url << endl;
    for (int k = 0; k < 40; k++)
        fout << "-";
    fout << endl;
    fout << Content << endl;
    fout.close();
}
