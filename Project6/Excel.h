#include <iostream>
#include <string>
#include "Document.h"
using namespace std;

class Excel :public Document// a child class to the Document class
{
public:
    Excel();// default constructor
    void setRow(int row);//accepts a row and sets row
    int getRow();// returns an int row
    void setColumn(int row);// accepts in integer and sets column
    int getColumn();// returns a column
    void readdata(string file);// reads the data from the input file the function accepts
    void printdata(string file);// prints the data to the output file the funcion accepts
private:
    int row;
    int column;
    double excelfile[50][50];
};
