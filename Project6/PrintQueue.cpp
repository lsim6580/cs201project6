#include <iostream>
#include <fstream>
#include "Document.h"
#include "PrintQueue.h"
using namespace std;

PrintQueue::PrintQueue()
{
    numberOfDocs = 0;
}
PrintQueue::~PrintQueue()
{
    for (int k = 0; k < numberOfDocs; k++)
    {
        delete printArray[k];
    }
}
void PrintQueue::addDocument(Document *D)
{
    printArray[numberOfDocs] = D;
    numberOfDocs += 1;
}
void PrintQueue::deleteDocuments()
{
    for (int k = 0; k < numberOfDocs; k++)
        delete printArray[k];
    numberOfDocs = 0;

}
void PrintQueue::sendDocuments()
{
    for (int k = 0; k < numberOfDocs; k++)
    {
        printArray[k]->printdata("printoutput.txt");
    }
    deleteDocuments();
}