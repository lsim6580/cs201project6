#include <iostream>
#include <fstream>
#include <string>
#include "Document.h"
using namespace std;

class Word :public Document// child class of the Document class
{
public:
    Word();//default constructor
    void setAuthor(string name);//accepts a string and sets Author
    string getAuthor();// returns Author
    void readdata(string file);//reads the data from the input file that the functions accepts
    void printdata(string file);// outputs the corrected format to the output file
private:
    string author;
};