#include <iostream>
#include <fstream>
#include <string>
#include "Document.h"
using namespace std;

class Html :public Document//Child class of Document
{
public:
    Html();// default constructor
    string getUrl();// returns Url
    void setUrl(string word);//accepts a string and sets url
    void readdata(string file);// read data will open the file and read its contents to the class
    void printdata(string file);// will output the data to an output file
private:
    string Url;
};