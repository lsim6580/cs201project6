#include <iostream>
#include <string>
#include "Document.h"
using namespace std;

Document::Document()
{
    type = "";
    Title = "";
    Content = "";
}
string Document::getType()
{
    return type;
}
string Document::getTitle()
{
    return Title;
}
string Document::getContent()
{
    return Content;
}
void Document::setType(string typef)
{
    type = typef;
}
void Document::setTitle(string type)
{
    Title = type;
}
void Document::setContent(string content)
{
    Content = content;
}