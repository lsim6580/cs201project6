#include <iostream>
#include <fstream>
#include "Document.h"
using namespace std;

class PrintQueue
{
public:
    PrintQueue();//Default constructor
    ~PrintQueue();// deconstructor
    void addDocument(Document *D);// accepts a pointer to Document and adds the document to the array
    void deleteDocuments();// deletes the documents from the array
    void sendDocuments();// sends the documents to the ouput file
private:
    Document *printArray[50];
    int numberOfDocs;
};
