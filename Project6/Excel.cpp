#include <iostream>
#include <fstream>
#include "Document.h"
#include "Excel.h"
using namespace std;

Excel::Excel()
{
    row = 0;
    column = 0;
    type = "Excel Document";
}
void Excel::setRow(int r)
{
    row = r;
}
int Excel::getRow()
{
    return row;
}
void Excel::setColumn(int r)
{
    column = r;
}
int Excel::getColumn()
{
    return column;
}
void Excel::readdata(string file)
{
    ifstream fin(file);
    double number;
    getline(fin, Title);
    fin >> row;
    fin >> column;
    for (int i = 0; i < row; i++)
    {
        for (int k = 0; k < column; k++)
        {
            fin >> number;

            if (number == '\t')
            {

            }
            else excelfile[i][k] = number;
        }
    }

    fin.close();
}
void Excel::printdata(string file)
{
    ofstream fout(file,ios::app);
    for (int k = 0; k < 40; k++)
        fout << "=";
    fout << endl;
    fout <<"Title: "<< Title << endl;
    fout << type << endl;
    for (int k = 0; k < 40; k++)
        fout << "-";
    fout << endl;
    for (int i = 0; i < row; i++)
    {
        for (int k = 0; k < column; k++)
            fout << excelfile[i][k] << '\t';
        fout << endl;
    }
    fout.close();
}
