#include <iostream>
#include <fstream>
#include <string>
#include "Document.h"
#include "Word.h"
using namespace std;

Word::Word()
{
    author = "";
    type = "Word Document";
}
void Word::setAuthor(string name)
{
    author = name;
}
string Word::getAuthor()
{
    return author;
}
void Word::readdata(string file)
{
    ifstream fin(file);
    string getcontent;
    getline(fin, Title);
    getline(fin, author);
    while (getline(fin, getcontent))
    {
        Content += getcontent + '\n';
    }
    fin.close();
}
void Word::printdata(string file)
{
    ofstream fout(file,ios::app);
    for (int k = 0; k < 40; k++)
        fout << "=";
    fout << endl;
    fout << "Title: " << Title << endl;
    fout << type << endl;
    fout << "Author: " << author << endl;
    for (int k = 0; k < 40; k++)
        fout << "-";
    fout << endl;
    fout << Content << endl;
    fout.close();
}