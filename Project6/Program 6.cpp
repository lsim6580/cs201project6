

/*********************************************

Name: 	Luke Simmons

Course:     CS 201
Program: 	Program 6
Due Date: 	4/19/2015
Description: 	Keep an array of files and either delete or print them.
Inputs: 	inputfile: will prompt the user for an input file
Outputs: 	fileoutput
Algorithm: 	
Prompt the user for the file of file names.
get the first word from the line.
if the word is ADD
    parse the word to get the extension
    if the extension is doc the file is a word document
        create a new instance of the Word class and read in the data and add the document to the print queue
    if the extension is htm the file is an Html document
        create a new instance of the Html Document and read the data and add the document to the print queue
    if the extension is xls the file is an excel document 
        create a new instance if the Excel class and read in the data and add the document to the print queue
if the word is PRINTALL
    output all of the documents to the output file.
if the word is CANCELALL 
    delete all of the documents in the print Array


*********************************************/


#include <iostream>
#include <fstream>
#include <string>
#include "Document.h"
#include "Word.h"
#include "Excel.h"
#include "Html.h"
#include "PrintQueue.h"
using namespace std;

int main()
{
    string getfile;
    string command;
    string filename;
    string extension;
    Document *k;
    int find;
    string name;
    PrintQueue queue;
    cout << "Enter the file name: " << endl;
    cin >> getfile;// input the file name
    ifstream fin(getfile);// open the file
    if (!(fin.good()))// check if the file is goos
    {
        cout << "file not found" << endl;
        system("pause");
        return 0;
    }
    while (fin >> command)// while there is still information to read in the file
    {
        if (command == "ADD")// if the first word is ADD
        {
            fin >> filename;
            find = filename.find('.');
            extension = filename.substr(find,find+4);// get the file type of the document
            name = filename.substr(0, find - 1);
            if (extension == ".doc")// if the file is a word document
            {
                k = new Word;
                k->readdata(filename);
                queue.addDocument(k);
            }
            if (extension == ".xls")// if the file is an Excel document
            {
                k = new Excel;
                k->readdata(filename);
                queue.addDocument(k);
            }
            if (extension == ".htm")// if the file is an Html document
            {
                k = new Html;
                k->readdata(filename);
                queue.addDocument(k);
            }
        
        }
        else if (command == "PRINTALL")// If the word is PRINTALL
        {
            queue.sendDocuments();
        }
        else if (command == "CANCELALL")// If the word is CANCELALL
        {
            queue.deleteDocuments();
        }
    }
    fin.close();
}