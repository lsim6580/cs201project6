#include <iostream>
#include <string>
#pragma once
using namespace std;

class Document// this is our parent class
{
public:
    Document();// default constructor
    string getType();// returns variable type
    string getTitle();//returns variable title
    string getContent();//returns the variable content
    void virtual readdata(string file)=0;// pure virtual readdata function makes this class abstract accepts input file name
    void virtual printdata(string file) = 0;// pure virtual print data function accepts output file.
    void setType(string type);//accepts a string sets variable type
    void setTitle(string type);//accepts a string sets variable title
    void setContent(string);// accepts a string sets variable Content.

protected:
    string type;
    string Title;
    string Content;

};